package com.company.service;

import com.company.model.UniClassModel;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DataService {
    //bot test veradardznox method vor@ karduma fileic u et id ov toxy veradardznuma

    // filey arden kardum, while-i mej
    public UniClassModel get(int id) throws Exception {
        final int EOF = -1; //EndOfFile
        final char fieldSeparator = '.';
        final char modelRowSeparator = '\n';
        int b;


        FileInputStream file = new FileInputStream("jetbrains://idea/navigate/reference?project=bot_test&path=resources/data.uu");
        List<Character> buffer = new ArrayList<>();
        int fieldCounter = 0;
        UniClassModel uniClassModel = new UniClassModel();
        List<UniClassModel> readObjects = new ArrayList<>();


        while ((b = file.read()) != EOF) {
            if (b == fieldSeparator) {
                String readField = bufferToString(buffer);
                buffer.clear();
                switch (fieldCounter) {
                    case 0:
                        uniClassModel.setId(readField);
                        break;
                    case 1:
                        uniClassModel.setProfName(readField);
                        break;
                    case 2:
                        uniClassModel.setPair(readField);
                        break;
                    case 3:
                        uniClassModel.setRoom(readField);
                        break;
                    default:
                        break;
                }
                fieldCounter++;
            } else if (b == modelRowSeparator) {
                bufferToString(buffer);
                buffer.clear();
                fieldCounter = 0;
                readObjects.add(uniClassModel);
                uniClassModel = new UniClassModel();
            } else {
                buffer.add((char) b);
            }
        }

        for (UniClassModel x : readObjects) {
            if (uniClassModel.getId() == Integer.toString(id)) {
                return x;
            }
        }
        return null;
    }

    private String bufferToString(List<Character> buffer) {
        return buffer.stream().map(Object::toString)
                .collect(Collectors.joining(""));
    }

}
